defmodule FD do
  @moduledoc """
  Documentation for FD.
  """

  @doc """
  Hello world.

  ## Examples

      iex> FD.hello
      :world

  """
  def hello do
    :world
  end
end
